package edu.kit.scc.dei.adsecp;

import org.apache.directory.api.ldap.model.entry.Entry;
import org.apache.directory.api.ldap.model.exception.LdapException;
import org.apache.directory.server.core.api.DirectoryService;
import org.apache.directory.server.core.api.filtering.EntryFilteringCursor;
import org.apache.directory.server.core.api.interceptor.BaseInterceptor;
import org.apache.directory.server.core.api.interceptor.context.AddOperationContext;
import org.apache.directory.server.core.api.interceptor.context.BindOperationContext;
import org.apache.directory.server.core.api.interceptor.context.CompareOperationContext;
import org.apache.directory.server.core.api.interceptor.context.DeleteOperationContext;
import org.apache.directory.server.core.api.interceptor.context.GetRootDseOperationContext;
import org.apache.directory.server.core.api.interceptor.context.HasEntryOperationContext;
import org.apache.directory.server.core.api.interceptor.context.LookupOperationContext;
import org.apache.directory.server.core.api.interceptor.context.ModifyOperationContext;
import org.apache.directory.server.core.api.interceptor.context.MoveAndRenameOperationContext;
import org.apache.directory.server.core.api.interceptor.context.MoveOperationContext;
import org.apache.directory.server.core.api.interceptor.context.OperationContext;
import org.apache.directory.server.core.api.interceptor.context.RenameOperationContext;
import org.apache.directory.server.core.api.interceptor.context.SearchOperationContext;
import org.apache.directory.server.core.api.interceptor.context.UnbindOperationContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class StatementLoggingInterceptor extends BaseInterceptor {

    private static final Logger LOG = LoggerFactory.getLogger(StatementLoggingInterceptor.class);

    private void log(OperationContext ctx) {
   		LOG.info("{}: {}", ctx.getEffectivePrincipal(), ctx);
    }
    
	@Override
	public void init(DirectoryService directoryService) throws LdapException {
		super.init(directoryService);
		
        LOG.info("StatementLoggingInterceptor has been initialized");
	}

	@Override
	public void add(AddOperationContext ctx) throws LdapException {
		log(ctx);
		next(ctx);
	}

	@Override
	public void bind(BindOperationContext ctx) throws LdapException {
		log(ctx);
        next(ctx);
	}

	@Override
	public boolean compare(CompareOperationContext ctx) throws LdapException {
		log(ctx);
		return next(ctx);
	}

	@Override
	public void delete(DeleteOperationContext ctx) throws LdapException {
		log(ctx);
        next(ctx);
	}

	@Override
	public Entry getRootDse(GetRootDseOperationContext ctx) throws LdapException {
		log(ctx);
		return next(ctx);
	}

	@Override
	public boolean hasEntry(HasEntryOperationContext ctx) throws LdapException {
		log(ctx);
		return next(ctx);
	}

	@Override
	public Entry lookup(LookupOperationContext ctx) throws LdapException {
		log(ctx);
		return next(ctx);
	}

	@Override
	public void modify(ModifyOperationContext ctx) throws LdapException {
		log(ctx);
        next(ctx);
	}

	@Override
	public void move(MoveOperationContext ctx) throws LdapException {
		log(ctx);
        next(ctx);
	}

	@Override
	public void moveAndRename(MoveAndRenameOperationContext ctx) throws LdapException {
		log(ctx);
        next(ctx);
	}

	@Override
	public void rename(RenameOperationContext ctx) throws LdapException {
		log(ctx);
        next(ctx);
	}

	@Override
	public EntryFilteringCursor search(SearchOperationContext ctx) throws LdapException {
		log(ctx);
        return next(ctx);
	}

	@Override
	public void unbind(UnbindOperationContext ctx) throws LdapException {
		log(ctx);
        next(ctx);
	}

	
}
